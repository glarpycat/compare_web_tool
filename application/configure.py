class Configure(object):
    _instance = None
    config = {
        'threads': 1,
        'url_encoding': 'cp932',
        'save_dir': 'output',
        # 'http_proxy': 'http://192.168.10.114:3128',
        # 'https_proxy': 'https://192.168.10.114:3128',
    }

    def __new__(cls, *args, **keys):
        if cls._instance is None:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self, save_dir=None):
        if save_dir:
            self.config['save_dir'] = save_dir

    def get(self, key, default=None):
        return self.config.get(key, default)
