import logging
import os

from application.configure import Configure
from infrastructure.tk.application import Application


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    configure = Configure()
    app = Application(configure)
    app()
