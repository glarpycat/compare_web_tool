from usecases.interactor_interface import InteractorInterface


class GenerateUrlInteractor(InteractorInterface):
    def __init__(self, presenter, input_data):
        self.presenter = presenter
        self.input_data = input_data

    def handle(self):
        self.generated_urls()

    def generated_urls(self):
        self.presenter.update_generated_urls(self.input_data.parameters)
