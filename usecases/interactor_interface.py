from abc import ABCMeta, abstractmethod


class InteractorInterface(metaclass=ABCMeta):
    @abstractmethod
    def handle(self):
        pass
