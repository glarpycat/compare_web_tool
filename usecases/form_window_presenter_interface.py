from abc import ABCMeta, abstractmethod


class FormWindowPresenterInterface(metaclass=ABCMeta):
    @abstractmethod
    def update_generated_urls(self, generated_urls, *args, **kwargs):
        pass
