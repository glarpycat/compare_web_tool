from abc import ABCMeta, abstractmethod


class WebRequestGatewayInterface(metaclass=ABCMeta):
    @abstractmethod
    def get_content(self, url, headers):
        pass

    @abstractmethod
    def set_request_detail(self, web_requests):
        pass


class HTTPError(Exception):
    def __init__(self, err):
        self.response = err
