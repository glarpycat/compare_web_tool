class GenerateUrlInputData(object):
    def __init__(self, parameters=None):
        if parameters:
            self.parameters = parameters
        else:
            self.parameters = []
    
    def add(self, request_parameters):
        self.parameters.append(request_parameters)
