import logging
import threading

from domain.user_agents import UserAgents
from domain.web_content import WebContent
from domain.web_requests import WebRequests
from usecases.interactor_interface import InteractorInterface
from usecases.web_request_gateway_interface import HTTPError


class GetWebContentslInteractor(InteractorInterface):
    def __init__(
            self,
            presenter,
            web_request_gateway,
            web_request_repository,
            web_content_repository,
            generated_urls,
            threads
        ):
        self.presenter = presenter
        self.web_request_gateway = web_request_gateway
        self.web_request_repository = web_request_repository
        self.web_content_repository = web_content_repository
        self.generated_urls = generated_urls
        self.threads = threads
        self.abort_getting_contents = False

    def handle(self):
        self.start_getting_contents()

    def start_getting_contents(self):
        self.presenter.start_getting_contents(self.generated_urls)
        if self.threads:
            thread = threading.Thread(target=self.get_contents)
            thread.start()
        else:
            self.get_contents()

    def get_contents(self):
        # コンテンツ取得
        self.abort_getting_contents = False
        web_requests = WebRequests()
        self.web_request_gateway.set_request_detail(web_requests)
        for gen_url in self.generated_urls:
            webcontent = WebContent(gen_url.url_id, gen_url.request_type, gen_url.url)
            web_requests.add(webcontent)
            self.presenter.update_get_url_contents(web_requests.contents)
            if self.abort_getting_contents:
                logging.info('abort getting contents: {0}'.format(gen_url.url))
                webcontent.abort()
                self.presenter.update_get_url_contents(web_requests.contents)
                return
            try:
                logging.info(gen_url.url)
                response = self.web_request_gateway.get_content(gen_url.url)
                webcontent.response = response
            except HTTPError as err:
                webcontent.response = err.response
            except Exception as err:
                webcontent.error = err
            finally:
                webcontent.done()
                self.presenter.update_get_url_contents(web_requests.contents)
        self.presenter.done_get_url_contents()

        # 取得結果を保存
        self.save(web_requests)

    def abort(self):
        self.abort_getting_contents = True

    def save(self, web_requests):
        self.web_request_repository.save(web_requests)
        self.web_content_repository.save(web_requests)
