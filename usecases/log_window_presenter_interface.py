from abc import ABCMeta, abstractmethod


class LogWindowPresenterInterface(metaclass=ABCMeta):
    @abstractmethod
    def set_view_model(self, data):
        pass

    @abstractmethod
    def start_getting_contents(self, *args, **kwargs):
        pass

    @abstractmethod
    def done_get_url_contents(self, *args, **kwargs):
        pass

    @abstractmethod
    def insert_log_windows(self, gen_url, response=None, error=None):
        pass

    @abstractmethod
    def update_get_url_contents(self, results, *args, **kwargs):
        pass
