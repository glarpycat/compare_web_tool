import datetime


class WebRequests(object):
    def __init__(self, browser_name=None, contents=None):
        if contents:
            self.contents = contents
        else:
            self.contents = []
        self.browser_name = browser_name
        self.create_time = datetime.datetime.now()

    def add(self, content):
        self.contents.append(content)
