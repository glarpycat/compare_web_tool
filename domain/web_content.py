import datetime
from enum import Enum


class Progress(Enum):
    WAIT = '待機中'
    PROCESSING = '取得中...'
    ABORT = '中断'
    DONE = '終了'


class RequestType(Enum):
    BEFORE = '旧'
    AFTER = '新'


class WebContent(object):
    def __init__(
            self,
            url_id,
            request_type,
            url,
            detail=None,
            response=None,
            error=None):
        self.url_id = url_id
        self.progress = Progress.PROCESSING
        self.request_type = request_type
        self.label = request_type.value
        self.url = url
        self.response = response
        self.error = error
        self.detail = detail
        self.create_time = datetime.datetime.now()
        self.update_time = None

    def processing(self):
        self.progress = Progress.PROCESSING
        self._renewal_update_time()

    def abort(self):
        self.progress = Progress.ABORT
        self._renewal_update_time()
    
    def done(self):
        self.progress = Progress.DONE
        self._renewal_update_time()

    def _renewal_update_time(self):
        self.update_time = datetime.datetime.now()
