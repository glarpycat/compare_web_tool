import urllib.parse
import urllib.request


class RequestParameters(object):
    def __init__(self, url_id, request_type, server, url, query, encoding):
        self.url_id = url_id
        self.request_type = request_type
        self.encoding = encoding
        self.url = self.generate_url(server, url, query)

    def generate_url(self, server, url, add_query):
        parsed_url = urllib.parse.urlparse(url)
        if not (server or parsed_url.scheme or parsed_url.netloc):
            return url
        parsed_server = urllib.parse.urlparse(server)
        replace_url = {
            'scheme': self._get_scheme(parsed_url, parsed_server),
            'netloc': self._get_netloc(parsed_url, parsed_server),
            'query': self._get_mixed_query_string(parsed_url, add_query),
        }
        return urllib.parse.urlunparse(parsed_url._replace(**replace_url))

    def _get_scheme(self, parsed_url, parsed_server):
        if parsed_server.scheme:
            return parsed_server.scheme
        if parsed_url.scheme:
            return parsed_url.scheme
        return 'https'

    def _get_netloc(self, parsed_url, parsed_server):
        if parsed_server.netloc:
            return parsed_server.netloc
        if parsed_server.path:
            return parsed_server.path
        if parsed_url.netloc:
            return parsed_url.netloc

    def _get_mixed_query_string(self, parsed_url, add_query):
        encoding = self.encoding
        query_dic = urllib.parse.parse_qs(parsed_url.query, encoding=encoding)
        add_query_dic = urllib.parse.parse_qs(add_query, encoding=encoding)
        query_dic.update(add_query_dic)
        return urllib.parse.urlencode(
            query_dic,
            True,
            encoding=self.encoding
        )
