from abc import ABCMeta, abstractmethod


class WebContentRepositoryInterface(metaclass=ABCMeta):
    @abstractmethod
    def save(self, web_requests):
        pass

    @abstractmethod
    def get_content_name(self, web_requests, content):
        pass

    @abstractmethod
    def get_header_name(self, web_requests, content):
        pass

    @abstractmethod
    def can_save(self, can_save, content):
        pass
