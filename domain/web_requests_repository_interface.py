from abc import ABCMeta, abstractmethod


class WebRequestsRepositoryInterface(metaclass=ABCMeta):
    @abstractmethod
    def save(self, web_requests):
        pass

    @abstractmethod
    def can_save(self):
        pass
