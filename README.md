# compare_web_tool
comparison tool written in Python/Tk.

## Requirements
- Python 3.6+

## Usage
```bash
$ git clone https://glarpycat@bitbucket.org/glarpycat/compare_web_tool.git
$ cd compare_web_tool
```

Execute following command.

```bash
$ python compare_web.py
```

## Contributing
Contributions, issues and feature requests are welcome.

## Author
- Bitbucket: [glarpycat](https://bitbucket.org/glarpycat/)

## License
This software is released under the MIT License.
