import datetime


class GettingContentsLog(object):
    def __init__(
            self,
            url_id,
            label,
            url,
            detail=None,
            response=None,
            error=None,
            create_time=None,
            update_time=None):
        self.url_id = url_id
        self.label = label
        self.url = url
        self.response = response
        self.error = error
        self.detail = detail
        self.create_time = create_time
        self.update_time = update_time

    @property
    def status(self):
        if self.error:
            return 'NG'
        if self.response:
            return 'OK' if self.response.code == 200 else 'NG'
        return ''

    @property
    def code(self):
        return self.response.code if self.response else ''

    @property
    def detail_label(self):
        if self.response:
            return self.response.reason
        elif self.error:
            return self.error
        return self.detail
    
    @property
    def log_id(self):
        return '{0},{1},{2}'.format(self.url_id, self.label, self.url)


class LogWindowViewModel(object):
    def __init__(self, getting_contents_logs=None):
        if getting_contents_logs is None:
            self.getting_contents_logs = []
        else:
            self.getting_contents_logs = getting_contents_logs

    def get_updated_contents(self, last_update=None):
        if last_update is None:
            return self.getting_contents_logs
        updated_contents_list = []
        for content in self.getting_contents_logs:
            if (content.update_time is None or
                    content.update_time > last_update):
                updated_contents_list.append(content)
                continue
        return updated_contents_list
