from interfaceadaptors.log_window_view_model import GettingContentsLog
from usecases.log_window_presenter_interface import LogWindowPresenterInterface


class LogWindowPresenter(LogWindowPresenterInterface):
    def __init__(self, view_model, callbacks):
        self.view_model = view_model
        self.callbacks = callbacks

    def set_view_model(self, view_model):
        self.view_model = view_model

    def start_getting_contents(self, generated_urls):
        self._call_callback('start_get_contents', generated_urls)

    def insert_log_windows(self, gen_url, response=None, error=None):
        self.callbacks.get('insert_log_windows')

    def update_get_url_contents(self, contents):
        getting_contents_logs = []
        for content in contents:
            getting_contents_logs.append(
                GettingContentsLog(
                    url_id=content.url_id,
                    label=content.label,
                    url=content.url,
                    detail=content.detail if content.detail else content.progress.value,
                    response=content.response,
                    error=content.error,
                    create_time=content.create_time,
                    update_time=content.update_time
                )
            )

        self.view_model.getting_contents_logs = getting_contents_logs
        self._call_callback('update_get_url_contents', getting_contents_logs)

    def done_get_url_contents(self):
        self._call_callback('done_get_url_contents')

    def _call_callback(self, name, *args, **kwargs):
        func = self.callbacks.get(name)
        func(*args, **kwargs)
