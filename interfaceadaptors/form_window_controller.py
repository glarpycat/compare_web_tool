from domain.request_parameters import RequestParameters
from domain.web_content import RequestType
from interfaceadaptors.form_window_presenter import FormWindowPresenter
from usecases.generate_url_input_data import GenerateUrlInputData
from usecases.generate_url_interactor import GenerateUrlInteractor


class FormWindowController(object):
    def __init__(self, view_model, callbacks):
        self.view_model = view_model
        self.callbacks = callbacks

    def update_generated_urls(
            self, 
            urls_str,
            before_server,
            before_query,
            after_server,
            after_query,
            encoding):
        server = {
            RequestType.BEFORE: before_server,
            RequestType.AFTER: after_server,
        }
        query = {
            RequestType.BEFORE: before_query,
            RequestType.AFTER: after_query,
        }
        input_data = GenerateUrlInputData()
        for url_id, url in enumerate(urls_str.splitlines(), 1):
            if not url:
                continue
            for request_type in (RequestType.BEFORE, RequestType.AFTER):
                input_data.add(
                    RequestParameters(
                        url_id,
                        request_type,
                        server[request_type],
                        url,
                        query[request_type],
                        encoding
                    )
                )
        presenter = FormWindowPresenter(self.view_model, self.callbacks)
        interactor = GenerateUrlInteractor(presenter, input_data)
        interactor.handle()
