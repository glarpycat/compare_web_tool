from domain.web_content import RequestType


class GeneratedUrl(object):
    def __init__(self, url_id, request_type, url):
        self.url_id = url_id
        self.request_type = request_type
        self.url = url
        self.label = self._get_label(request_type)

    def _get_label(self, request_type):
        if request_type is RequestType.BEFORE:
            return '旧'
        elif request_type is RequestType.AFTER:
            return '新'
        else:
            return '不明'

class FormWindowViewModel(object):
    def __init__(self, generated_urls=None):
        if generated_urls:
            self.generated_urls = generated_urls
        else:
            self.generated_urls = []

    def init(self):
        self.generated_urls = []
    
    def add(self, url_id, request_type, url):        
        self.generated_urls.append(
            GeneratedUrl(url_id, request_type, url)
        )

