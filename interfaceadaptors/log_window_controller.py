import logging

from domain.user_agents import UserAgents
from domain.web_requests import WebRequests
from infrastructure.file.web_requests_repository import WebRequestsRepository
from infrastructure.file.web_content_repository import WebContentRepository
from interfaceadaptors.log_window_presenter import LogWindowPresenter
from interfaceadaptors.web_request_gateway import WebRequestGateway
from usecases.get_web_contents_interactor import GetWebContentslInteractor


class LogWindowController(object):
    def __init__(
            self,
            view_model,
            callbacks,
            is_checked_save_headers,
            is_checked_new_line_normalization):
        self.view_model = view_model
        self.callbacks = callbacks
        self.can_save_headers = is_checked_save_headers
        self.can_normalize_new_line = is_checked_new_line_normalization
        self.can_save_logs = True
        self.can_save_contents = True
        self.interactor = None

    def start_getting_contents(
            self,
            generated_urls,
            browser_name,
            proxies=None,
            threads=0):
        presenter = LogWindowPresenter(self.view_model, self.callbacks)
        web_request_gateway = WebRequestGateway(browser_name, proxies)
        web_request_repository = WebRequestsRepository(self.can_save_logs)
        web_content_repository = WebContentRepository(
            can_save_contents=self.can_save_contents,
            can_save_headers=self.can_save_headers,
            can_normalize_new_line=self.can_normalize_new_line)
        self.interactor = GetWebContentslInteractor(
            presenter=presenter,
            web_request_gateway=web_request_gateway,
            web_request_repository=web_request_repository,
            web_content_repository=web_content_repository,
            generated_urls=generated_urls,
            threads=threads
        )
        self.interactor.handle()

    def abort_getting_contents(self):
        self.interactor.abort()
