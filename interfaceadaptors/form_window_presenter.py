from usecases.form_window_presenter_interface import FormWindowPresenterInterface


class FormWindowPresenter(FormWindowPresenterInterface):
    def __init__(self, view_model, callbacks):
        self.view_model = view_model
        self.callbacks = callbacks

    def update_generated_urls(self, data):
        self.view_model.init()
        for param in data:
            self.view_model.add(param.url_id, param.request_type, param.url)
        self._call_callback('update_generated_urls', self.view_model)

    def _call_callback(self, name, *args, **kwargs):
        func = self.callbacks.get(name)
        func(*args, **kwargs)
