import urllib.parse
import urllib.request
from domain.user_agents import UserAgents
from usecases.web_request_gateway_interface import WebRequestGatewayInterface, HTTPError


class WebRequestGateway(WebRequestGatewayInterface):
    def __init__(self, browser_name, proxies):
        self.browser_name = browser_name
        self.proxies = proxies if proxies else {}
        self.user_agent = UserAgents().get_user_agent(browser_name)

    def get_content(self, url):
        if self.proxies:
            proxy_handler = urllib.request.ProxyHandler(self.proxies)
            opener = urllib.request.build_opener(proxy_handler)
            urllib.request.install_opener(opener)
        headers = {'User-Agent': self.user_agent}
        try:
            request = urllib.request.Request(url, None, headers)
            response = urllib.request.urlopen(request)
        except urllib.error.HTTPError as err:
            raise HTTPError(err)
        return response

    def set_request_detail(self, web_requests):
        web_requests.browser_name = self.browser_name
