import tkinter as tk
import tkinter.scrolledtext
import tkinter.ttk
import urllib.parse
import urllib.request
import logging

from domain.user_agents import UserAgents
from infrastructure.tk.log_window import LogWindow
from interfaceadaptors.form_window_controller import FormWindowController
from interfaceadaptors.form_window_view_model import FormWindowViewModel
from interfaceadaptors.form_window_presenter import FormWindowPresenter

class FormWindow(tk.Frame):

    def __init__(self, master, configure):
        super().__init__(master)
        self.pack(fill=tk.BOTH, expand=True, padx=10, pady=10)
        self.view_model = FormWindowViewModel()
        self.controller = FormWindowController(
            view_model = self.view_model,
            callbacks = {
                'update_generated_urls':
                    self.callback_update_generated_urls,
            }
        )
        self.configure = configure
        self.create_widgets()

    def create_widgets(self):
        # PanedWindow ラッパー用
        panedwindow_wrapper = tk.PanedWindow(self, orient='vertical')
        # PanedWindow 入力用(上部)
        panedwindow_controller = tk.PanedWindow(
            panedwindow_wrapper,
            borderwidth=1,
            # relief="sunken"
        )
        panedwindow_controller.columnconfigure(1, weight=1)
        panedwindow_controller.columnconfigure(2, weight=1)
        panedwindow_controller.rowconfigure(5, weight=1)

        # UserAgent
        label_user_agent = tk.Label(panedwindow_controller, text='UserAgent')
        label_user_agent.grid(sticky='nw')
        user_agents = UserAgents()
        self.combobox_user_agent = tk.ttk.Combobox(
            panedwindow_controller,
            value=user_agents.get_browser_list()
        )
        self.combobox_user_agent.set(user_agents.get_browser_list()[0])
        self.combobox_user_agent.grid(row=0, column=1, sticky='w')

        # ラベル(旧)
        label_before = tk.Label(panedwindow_controller, text='before')
        label_before.grid(row=1, column=1, sticky='w')

        # ラベル(新)
        label_after = tk.Label(panedwindow_controller, text='after')
        label_after.grid(row=1, column=2, sticky='w')

        # ラベル(ポート)
        label_query_group = tk.Label(
            panedwindow_controller,
            text='スキーマ、サーバー(ポート)'
        )
        label_query_group.grid(sticky='w')

        # Entry ポート(旧)
        self.entry_server_before = tk.Entry(panedwindow_controller, width=30)
        self.entry_server_before.grid(row=2, column=1, sticky='we')
        self.entry_server_before.bind('<KeyRelease>', self.on_change_server_entry)
        # Entry ポート(新)
        self.entry_server_after = tk.Entry(panedwindow_controller)
        self.entry_server_after.grid(row=2, column=2, sticky='we')
        self.entry_server_after.bind('<KeyRelease>', self.on_change_server_entry)

        # クエリパラメータ
        label_query_group = tk.Label(
            panedwindow_controller,
            text='クエリパラメータ'
        )
        label_query_group.grid(sticky='w')
        # クエリパラメータ(旧)
        self.entry_query_before = tk.Entry(panedwindow_controller, width=30)
        self.entry_query_before.grid(row=3, column=1, sticky='we')
        self.entry_query_before.bind('<KeyRelease>', self.on_change_query_entry)
        # クエリパラメータ(新)
        self.entry_query_after = tk.Entry(panedwindow_controller)
        self.entry_query_after.grid(row=3, column=2, sticky='we')
        self.entry_query_after.bind('<KeyRelease>', self.on_change_query_entry)

        # 入力用 共通URL テキストエリア
        label_urls = tk.Label(panedwindow_controller, text='共通パス')
        label_urls.grid(sticky='w')
        self.scrolled_text_urls = tk.scrolledtext.ScrolledText(
            panedwindow_controller,
            height='10'
        )
        self.scrolled_text_urls.grid(columnspan=3, sticky='nwse')
        self.scrolled_text_urls.bind(
            '<KeyRelease>',
            self.on_change_scrolled_text_urls
        )

        # Checkbutton レスポンスヘッダー保存の有無
        self.is_checked_save_headers = tk.BooleanVar()
        checkbutton_save_headers = tk.Checkbutton(
            panedwindow_controller,
            variable=self.is_checked_save_headers,
            text='レスポンスヘッダーの保存')
        checkbutton_save_headers.grid()

        # 改行コードを統一する
        self.is_checked_new_line_normalization = tk.BooleanVar(value=True)
        checkbutton_new_line_normalization = tk.Checkbutton(
            panedwindow_controller,
            variable=self.is_checked_new_line_normalization,
            text='改行コードを統一する(LF)'
        )
        checkbutton_new_line_normalization.grid()

        # PanedWindows URLs出力用(下部)
        panedwindow_urls = tk.PanedWindow(panedwindow_wrapper)
        panedwindow_urls.columnconfigure(0, weight=1)
        panedwindow_urls.rowconfigure(1, weight=1)

        # TreeView 生成URLs
        label_urls = tk.Label(panedwindow_urls, text='生成URLs')
        label_urls.grid(sticky='w')
        self.treeview_urls = tk.ttk.Treeview(
            panedwindow_urls,
            column=(1, 2, 3)
        )
        self.treeview_urls.column(1, width=40)
        self.treeview_urls.column(2, width=40)
        self.treeview_urls.column(3, width=500)
        self.treeview_urls['show'] = 'headings'
        self.treeview_urls.heading(1, text='ID')
        self.treeview_urls.heading(2, text='種類')
        self.treeview_urls.heading(3, text='URL')
        self.treeview_urls.grid(sticky='nwse')
        scrollbar_y = tk.ttk.Scrollbar(
            panedwindow_urls,
            orient=tk.VERTICAL,
            command=self.treeview_urls.yview
        )
        scrollbar_y.grid(row=1, column=1, sticky='nes')
        self.treeview_urls.configure(yscroll=scrollbar_y.set)

        # Webコンテンツ取得ボタン
        self.button_get_url_contents = tk.Button(
            panedwindow_urls, text='取得',
            state='disable',
            command=self.on_click_button_get_url_contents,
        )
        self.button_get_url_contents.grid(sticky='wes')

        # PanedWindowの生成
        panedwindow_wrapper.pack(expand=True, fill=tk.BOTH)
        panedwindow_controller.pack(fill=tk.BOTH, expand=True)
        panedwindow_urls.pack(fill=tk.BOTH, expand=True)
        panedwindow_wrapper.add(panedwindow_controller)
        panedwindow_wrapper.add(panedwindow_urls)

    def on_change_server_entry(self, event):
        txt = event.widget.get().strip()
        if not txt:
            self.update_generated_urls()
            return
        parsed_url = urllib.parse.urlparse(txt)
        if parsed_url.scheme and parsed_url.netloc and parsed_url.path:
            server = '{server}://{netloc}'.format(
                server=parsed_url.scheme,
                netloc=parsed_url.netloc,
            )
            event.widget.delete(0, tk.END)
            event.widget.insert(0, server)
        self.update_generated_urls()

    def on_change_query_entry(self, event):
        txt = event.widget.get().strip()
        if not txt:
            self.update_generated_urls()
            return
        parsed_url = urllib.parse.urlparse(txt)
        if not parsed_url.query:
            self.update_generated_urls()
            return
        event.widget.delete(0, tk.END)
        event.widget.insert(0, parsed_url.query)
        self.update_generated_urls()

    def on_change_scrolled_text_urls(self, event):
        self.update_generated_urls()

    def on_click_button_get_url_contents(self):
        self.log_window = LogWindow(
            self.is_checked_save_headers.get(),
            self.is_checked_new_line_normalization.get(),
            self.configure)
        self.log_window.minsize(800, 400)
        self.log_window.focus_set()
        self.log_window.transient(self)
        self.log_window.grab_set()
        self.log_window.start_get_contents(
            self.view_model.generated_urls,
            self.combobox_user_agent.get())

    def update_generated_urls(self):
        self.controller.update_generated_urls(
            self.scrolled_text_urls.get('1.0', 'end -1c'),
            self.entry_server_before.get(),
            self.entry_query_before.get(),
            self.entry_server_after.get(),
            self.entry_query_after.get(),
            self.configure.get('url_encoding', 'cp932')
        )

    def clear_treeview_urls(self):
        for child in self.treeview_urls.get_children():
            self.treeview_urls.delete(child)

    def callback_update_generated_urls(self, view_model, *args, **kwargs):
        self.clear_treeview_urls()
        for gen_url in view_model.generated_urls:
            self.treeview_urls.insert(
                '',
                'end',
                values=(gen_url.url_id, gen_url.label, gen_url.url)
            )
        if len(view_model.generated_urls) > 0:
            self.button_get_url_contents.config(state='active')
        else:
            self.button_get_url_contents.config(state='disable')


