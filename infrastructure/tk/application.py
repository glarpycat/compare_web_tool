import tkinter as tk

from infrastructure.tk.form_window import FormWindow


class Application(object):
    def __init__(self, configure):
        self.configure = configure
    
    def __call__(self):
        root = tk.Tk()
        root.title('前後比較')
        root.minsize(600, 700)
        window = FormWindow(master=root, configure=self.configure)
        window.mainloop()
