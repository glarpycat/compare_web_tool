import logging
import tkinter as tk
import tkinter.scrolledtext
import tkinter.ttk
from itertools import zip_longest

from infrastructure.tk.web_requests_log_tree_view import WebRequestsLogTreeView
from interfaceadaptors.log_window_controller import LogWindowController
from interfaceadaptors.log_window_view_model import LogWindowViewModel


class LogWindow(tk.Toplevel):
    def __init__(self, is_checked_save_headers, is_checked_new_line_normalization, configure, master=None):
        super().__init__(master)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(1, weight=1)
        self.view_model = LogWindowViewModel()
        self.controller = LogWindowController(
            view_model=self.view_model,
            callbacks={
                'start_get_contents':
                    self.callback_start_get_contents,
                'update_get_url_contents':
                    self.callback_update_get_url_contents,
                'done_get_url_contents':
                    self.callback_done_get_url_contents,
            },
            is_checked_save_headers=is_checked_save_headers,
            is_checked_new_line_normalization=is_checked_new_line_normalization
        )
        self.configure = configure
        self.generated_urls = 0
        self.done_urls = 0
        self.create_widgets()

    def create_widgets(self):
        # Label(取得状態表示用ラベル)
        self.label_urls = tk.Label(
            self,
            text=self.get_progress_label()
        )
        self.label_urls.grid(sticky='e')

        # Treeview(取得ログ用)
        self.treeview_urls = WebRequestsLogTreeView(self)

        # 中断ボタン
        self.button_abort = tk.Button(
            self, text='中断', command=self.on_click_button_abort
        )
        self.button_abort.grid(stick='se')

    def start_get_contents(self, generated_urls, user_agent):
        try:
            proxies = {}
            if self.configure.get('http_proxy'):
                proxies.update({'http': self.configure.get('http_proxy')})
            if self.configure.get('https_proxy'):
                proxies.update({'https': self.configure.get('https_proxy')})
            self.controller.start_getting_contents(
                generated_urls,
                user_agent,
                proxies,
                self.configure.get('threads')
            )
        except Exception as err:
            logging.warn(err)

    def update_treeview_urls(self):
        self.label_urls['text'] = self.get_progress_label()
        last_update_time = self.treeview_urls.last_update_time
        for log in self.view_model.get_updated_contents(last_update_time):
            self.treeview_urls.update_tree_view(log)

    def get_progress_label(self):
        return '{0:2d}/{1}'.format(self.done_urls, self.generated_urls)

    def on_click_button_abort(self):
        logging.info('click abort button: please wait for aborting')
        self.controller.abort_getting_contents()

    def callback_start_get_contents(self, generated_urls, *args, **kwargs):
        self.generated_urls = len(generated_urls)
        self.update_treeview_urls()

    def callback_done_get_url_contents(self, *args, **kwargs):
        self.button_abort.config(state='disable')

    def callback_update_get_url_contents(self, getting_contents_logs, *args, **kwargs):
        self.done_urls = len(getting_contents_logs)
        self.update_treeview_urls()
