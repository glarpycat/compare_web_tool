import datetime
import tkinter as tk
# import tkinter.ttk


class WebRequestsLogTreeView(tk.ttk.Treeview):
    def __init__(self, master):
        self._items = {}
        self.last_update_time = None

        super().__init__(
            master,
            column=(1, 2, 3, 4, 5, 6)
        )
        self.column(1, width=10)
        self.column(2, width=10)
        self.column(3, width=10)
        self.column(4, width=400)
        self.column(5, width=25)
        self['show'] = 'headings'
        self.heading(1, text='結果')
        self.heading(2, text='ID')
        self.heading(3, text='種類')
        self.heading(4, text='URL')
        self.heading(5, text='CODE')
        self.heading(6, text='詳細')
        self.grid(sticky='nwse')
        scrollbar_y = tk.ttk.Scrollbar(
            master,
            orient=tk.VERTICAL,
            command=self.yview
        )
        scrollbar_y.grid(row=1, column=1, sticky='ns')
        self.configure(yscroll=scrollbar_y.set)

    def insert_log(self, log):
        self._items[log.log_id] = self.insert(
            '',
            'end',
            values=(
                log.status,
                log.url_id,
                log.label,
                log.url,
                log.code,
                log.detail_label
            )
        )

    def update_tree_view(self, log):
        item = self._items.get(log.log_id)
        if item is None:
            self.insert_log(log)
        else:
            self.set(item, 1, log.status)
            self.set(item, 2, log.url_id)
            self.set(item, 3, log.label)
            self.set(item, 4, log.url)
            self.set(item, 5, log.code)
            self.set(item, 6, log.detail_label)
        self._update_last_update_time()

    def _update_last_update_time(self):
        self.last_update_time = datetime.datetime.now()
