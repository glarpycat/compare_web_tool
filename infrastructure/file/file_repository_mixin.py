import logging
import os

from application.configure import Configure


class FileRepositoryMixin(object):
    @staticmethod
    def get_root_dir(web_requests):
        save_dir = Configure().get('save_dir')
        dir_name = web_requests.create_time.strftime('%Y%m%d-%H%M%S')
        return os.path.join(save_dir, dir_name)

    @classmethod
    def make_dir_if_not_exists(cls, dir_name):
        if not os.path.isdir(dir_name):
            logging.info('make dir:' + dir_name)
            os.makedirs(dir_name)

    @classmethod
    def write_file(cls, save_dir, save_file, data, encoding='utf-8', is_new_line_normalization=False):
        cls.make_dir_if_not_exists(save_dir)
        save_file_path = os.path.join(save_dir, save_file)
        with open(save_file_path, 'w', encoding=encoding) as f:
            if is_new_line_normalization:
                data = data.replace('\r', '')   
            f.write(data)
