import logging
import os

from domain.web_requests_repository_interface import WebRequestsRepositoryInterface
from infrastructure.file.file_repository_mixin import FileRepositoryMixin


class WebRequestsRepository(WebRequestsRepositoryInterface, FileRepositoryMixin):
    def __init__(self, can_save_log=True, save_file='requests.log'):
        self.can_save_log = can_save_log
        self.save_file = save_file

    def save(self, web_requests):
        if not self.can_save():
            logging.info('skip save log: {0}'.format(self.save_file))
            return
        logging.info('save log: {0}'.format(self.save_file))
        data = '\n'.join(
            map(
                lambda content: '{0}\t{1}\t{2}'.format(
                    content.create_time.strftime('%Y-%m-%d %H:%M:%S'),
                    'ERROR' if content.error else content.response.code,
                    content.url
                ),
                web_requests.contents
            )
        )
        FileRepositoryMixin.write_file(
            FileRepositoryMixin.get_root_dir(web_requests),
            self.save_file,
            data
        )

    def can_save(self):
        return self.can_save_log
