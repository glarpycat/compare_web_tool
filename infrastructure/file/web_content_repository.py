import logging
import os

from domain.web_content import RequestType
from domain.web_content_repository_interface import WebContentRepositoryInterface
from infrastructure.file.file_repository_mixin import FileRepositoryMixin


class WebContentRepository(WebContentRepositoryInterface, FileRepositoryMixin):
    SAVE_DIR_BEFORE = 'before'
    SAVE_DIR_AFTER = 'after'

    def __init__(
            self,
            can_save_contents=True,
            can_save_headers=False,
            can_normalize_new_line=False):
        self.can_save_contents = can_save_contents
        self.can_save_headers = can_save_headers
        self.can_normalize_new_line = can_normalize_new_line
        self.log_dir = None

    def save(self, web_requests):
        self.log_dir = FileRepositoryMixin.get_root_dir(web_requests)
        for content in web_requests.contents:
            self.save_content(
                content,
                self.get_content_name(web_requests, content)
            )
            self.save_header(
                content,
                self.get_header_name(web_requests, content)
            )

    def get_content_name(self, web_requests, content):
        return 'test-{0}-{1}.html'.format(web_requests.browser_name, content.url_id)

    def get_header_name(self, web_requests, content):
        return 'test-{0}-{1}.txt'.format(web_requests.browser_name, content.url_id)

    def can_save(self, can_save, content):
        if not can_save:
            return False
        if content.error:
            return False
        if content.response.code != 200:
            return False
        return True

    def save_header(self, content, save_file):
        if not self.can_save(self.can_save_headers, content):
            logging.warn('skip save headers: {0}'.format(save_file))
            return
        data = '\n'.join(
            map(
                lambda header: '{0}:\t{1}'.format(*header),
                content.response.getheaders()
            )
        )
        FileRepositoryMixin.write_file(
            self._get_save_dir(content),
            save_file,
            data
        )

    def save_content(self, content, save_file):
        if not self.can_save(self.can_save_contents, content):
            logging.warn('skip save contents: {0}'.format(save_file))
            return
        response = content.response
        charset = response.headers.get_content_charset()
        charset = charset if charset else 'cp932'
        FileRepositoryMixin.write_file(
            save_dir=self._get_save_dir(content),
            save_file=save_file,
            data=response.read().decode(charset),
            encoding=charset,
            is_new_line_normalization=self.can_normalize_new_line
        )

    def _get_save_dir(self, content):
        save_dir_mapping = {
            RequestType.BEFORE: self.SAVE_DIR_BEFORE,
            RequestType.AFTER: self.SAVE_DIR_AFTER
        }
        return os.path.join(
            self.log_dir, 
            save_dir_mapping.get(content.request_type)
        )
