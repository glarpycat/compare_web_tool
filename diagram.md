# Markdown + PlantUML Test

## Chaper 1.

PlantUMLのクラス図を直接Markdownに埋め込める。

## クラス図
```plantuml
@startuml

package application {
    class Configure {
        -{static} _instance
        -{static} config
        + get()
    }
}

package domain {
    class UserAgents {
        -{static} USERAGENT
        + get_browser_list()
        + get_user_agent(browser_name)
    }
    class Progress
    class RequestType
    class RequestParameters {
        + url_id
        + request_type
        + encoding
        + url
        + generate_url()
    }
    class WebContent {
        + url_id
        + request_type
        + url
        + detail
        + response 
        + error
        + label
        + progress
        + create_time
        + abort()
        + done()
    }
    class WebRequests {
        + contents
        + browser_name
        + create_time
        + add()
    }
    interface WebContentRepositoryInterface {
        +{abstract} save()
        +{abstract} get_content_name()
        +{abstract} get_header_name()
        +{abstract} can_save()
    }
    interface WebRequestsRepositoryInterface {
        +{abstract} save()
        +{abstract} can_save()
    }
    RequestParameters -- RequestType
    WebContent - RequestType
    Progress -- WebContent
    RequestParameters -[hidden]- WebContentRepositoryInterface
    RequestType -left[hidden]-WebRequests
    WebRequestsRepositoryInterface -[hidden] WebContentRepositoryInterface
}

package usecases {
    interface InteractorInterface {
        +{abstract} handle()
    }
    interface FormWindowPresenterInterface
    interface LogWindowPresenterInterface
    interface WebRequestGatewayInterface {
        +{abstract} get_content()
        +{abstract } set_request_detail()
    }
    class HTTPError
    class GenerateUrlOutputData
    class GenerateUrlInteractor {
        + presenter
        + input_data
        + handle()
        + generated_urls()

    }
    class GetWebContentslInteractor {
        - presenter,
        - web_request_gateway,
        - web_request_repository
        - web_content_repository
        - generated_urls
        - threads 
        - abort_getting_contents
        + handle()
        + abort()
        - start_getting_contents()
        - get_contents()
        - save()
    }
    GenerateUrlInteractor --> FormWindowPresenterInterface : handle()
    GenerateUrlInteractor ..|> InteractorInterface 
    GenerateUrlInteractor -> GenerateUrlOutputData
    GetWebContentslInteractor ..|>InteractorInterface
    WebRequests <-- GetWebContentslInteractor : add()
    WebContent <-- GetWebContentslInteractor : abort()\ndone()
    GetWebContentslInteractor - HTTPError
    GetWebContentslInteractor --> LogWindowPresenterInterface : start_getting_contents()\nupdate_get_url_contents()\ndone_get_url_contents()
    GetWebContentslInteractor --> WebRequestGatewayInterface : set_request_detail()\nget_content()
    WebRequestsRepositoryInterface <-- GetWebContentslInteractor : save()
    WebContentRepositoryInterface <-- GetWebContentslInteractor : save()
}

package interfaceadaptors {
    class FormWindowController {
        + view_model
        + callbacks
        + update_generated_urls(self, urls_str, before, after, encoding)
    }
    class FormWindowPresenter {
        + view_model
        + callbacks
        + update_generated_urls(data)
        - _call_callback
    }
    class FormWindowViewModel
    class LogWindowController {
        - view_model
        - callbacks
        - can_save_headers
        - can_save_logs
        - can_save_contents
        - interactor
        + start_getting_contents()
    }
    class LogWindowPresenter
    class LogWindowViewModel
    class WebRequestGateway {
        + browser_name
        + proxies
        + user_agent
        + get_content()
        + set_request_detail()
    }
    WebRequestGatewayInterface <|.. WebRequestGateway 
    LogWindowController -> LogWindowPresenter
    WebRequestGatewayInterface <-- LogWindowController
    WebRequestsRepositoryInterface <-- LogWindowController
    WebContentRepositoryInterface <-- LogWindowController 
    InteractorInterface <-- LogWindowController : handle()

    FormWindowController --> RequestType
    FormWindowController --> GenerateUrlInputData
    FormWindowController -> FormWindowPresenter
    InteractorInterface <-- FormWindowController : handle()
    LogWindowPresenterInterface <|.. LogWindowPresenter
    FormWindowPresenterInterface <|.. FormWindowPresenter
}

package infrastructure {
    package file {
        class FileRepositoryMixin {
            +{static} get_root_dir()
            +{static} make_dir_if_not_exists()
            +{static} write_file()
        }
        class WebContentRepository {
            - can_save_contents
            - can_save_headers
            - log_dir
            + save()
            + get_content_name()
            + get_header_name()
            + can_save()
            + save_header()
            + save_content()
            - _get_save_dir()
        }
        class WebRequestsRepository {
            - can_save_log
            - save_file
            + save()
            + can_save()
        }
        WebContentRepositoryInterface <|.. WebContentRepository 
        WebContentRepository --|> FileRepositoryMixin
        WebRequestsRepositoryInterface <|.. WebRequestsRepository
        WebRequestsRepository --|> FileRepositoryMixin
    }

    package tk {
        class Application {
            + configure
        }
        class LogWindow {
            - view_model
            - controller
            - is_checked_save_headers
            - generated_urls
            - done_urls
            - configure
            + start_get_contents()
            + callback_start_get_contents()
            + callback_done_get_url_contents()
            + callback_update_get_url_contents()
            - create_widgets()
            - _insert()
            - _update()
            - clear_treeview_urls()
            - update_treeview_urls()
            - get_progress_label()
            - on_click_button_abort()
        }
        LogWindowViewModel -- LogWindow
        Configure <- LogWindow
        LogWindowController <-- LogWindow : start_get_contents()\nabort_getting_contents()
        class FormWindow {
            - log_window
            - controller
            - view_model
            - configure
            + callback_update_generated_urls()
            - create_widgets()
            - on_change_entry()
            - on_change_scrolled_text_urls()
            - on_click_button_get_url_contents()
            - on_click_button_get_url_contents()
            - update_generated_urls()
            - clear_treeview_urls()
        }
        FormWindow <-- Application : mainloop()
        UserAgents <-- FormWindow
        Configure <- FormWindow 
        LogWindow <-- FormWindow : start_get_contents()
        FormWindowController <-- FormWindow : update_generated_urls
        FormWindowViewModel -- FormWindow
        WebRequestGateway -down[hidden]- FormWindowViewModel
        LogWindowViewModel -left[hidden]- FormWindowViewModel
    }
}

Configure <---- compare_web
Application <--- compare_web 

@enduml
```
